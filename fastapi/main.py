from fastapi import FastAPI
from third_party.fahslotinfo import parse
from string import Template
import socket

app = FastAPI()

addrlist = ["192.168.7.77", "ub-bhyve.hell", "192.168.1.1", "192.168.5.55"]

@app.get('/info')
def index():
	return {"service_name": "fahinfo"}

@app.get('/')
def fahslotinfo():
	port = 36330

	resdict = {}
	for addr in addrlist:
		try:
			fahsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			fahsock.connect((addr, port))
			data = fahsock.recv(4096)
			fahsock.sendall(b"hello\n")
			data = fahsock.recv(4096)
			fahsock.sendall(b"slot-info\n")
			data = fahsock.recv(4096)
			fahsock.close()
			ndata = data.decode("utf-8")
			value = parse(ndata)
			resdict.__setitem__(addr, value)
		except:
			tmpl = Template("something went wrong with host $host)")
			print(tmpl.substitute(host=addr))

	return resdict
