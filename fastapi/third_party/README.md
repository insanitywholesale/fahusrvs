# 3rd party
This directory contains ("vendors") modified copies of the following repositories:

- [insanitywholesale/snaking -- fahslotinfo.py](https://gitlab.com/insanitywholesale/snaking/-/blob/e0bb7e62e5b6960db221f33041508ab1e5897784/fahslotinfo.py)

# don't sue me
See the respective LICENSE files of each project for the applicable license terms.
